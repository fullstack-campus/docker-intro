'use strict';

const http = require('http');
const axios = require('axios')

const port = 3000;

const server = http.createServer(async (req, res) => {
  const backendReq = await axios.get('http://backend:3000')
  res.end('Simon sais: ' + backendReq.data)
});

server.listen(port, () => {
  console.log(`Server running at http://${port}/`);
});

process.on('SIGINT', () => {
  console.log('Goodbye cruel world!')
  process.exit(0)
})
process.on('SIGTERM', () => {
  console.log('Goodbye cruel world!')
  process.exit(0)
})
