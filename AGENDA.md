1. Vagrant VM -> attached volume

2. install docker

3. docker run -it alpine sh

4. alpine chroot

5. docker run -it alpine sh
  1. show env
  2. show volume

6. create node hello server container
  1. show port forwarding
  2. show docker ps
  3. show docker ps kill
  4. show docker images
  5. htop docker and node process
  6. push to registry

7. overlay fs: mount -t overlay

8. node with env

9. node with volume

10. docker-compose.yml for networking

11. scratch container for go
  1. building without CGO flag
  2. show an empty chroot with go

